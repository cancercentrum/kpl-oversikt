# README #

Skapa Koll på läget-översikt på INCA

### Manual pdf ###

Manual i PDF-format finns under Downloads

### Script ###

KPL_oversikt_exempel.R innehåller exempelkod från Huvud-hals registret för att skapa KPL-översikt på INCA.

KPL_oversikt_mall.R är en mall som innehåller grundkod att bygga vidare på för att skapa registerspecifik KPL-översikt på INCA.

Funktionerna KPL_oversikt_fun.R och plotInd_lokal.R behöver läsas in för att kunna skapa översikten.

### Kontakt ###

Vid frågor kontakta Madeleine Helmersson 